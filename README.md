# Go-Forum

### groupe :
- ROUSSAT David
- MURARO Arthur
- AUGIER DE MOUSSAC Clément
- QUESNOY Théo

### Présentation du Projet :

Go-Forum est un projet Web dévelloppé en Golang exploitant une base de donnée SQLite3.
Comme tout forum, il permet de partager ses centres d'intêrets. une fois authentifié un utilisateur a la possibilité de poster, Liker, commenter, et personnaliser ses information ou son avatar et ce de manière sécurisé sur la base de donnée grâce au chiffrement.

### procédure d'installation :

Le dossier appelé "src" contient l'intégralité du code ainsi que les
fichiers indispensables au bon fonctionnement du projet.
Il est donc important de copier tout le dossier.
    
Ce dossier contient un fichier "Dockerfile" il vous permettra de créer une
image Docker qui contiendra l'executable du programme.
    
    
Procédure d'installation réelle en considérant que Docker ( https://docs.docker.com/engine/install/ ) soit installé sur votre machine et que vous soyez sous linux ou macOS :
    
        $ git clone https://git.ytrack.learn.ynov.com/CAUGIERDEMOUSSAC/Forum.git

        $ ./Forum/src/CreateAndRunGoForum
        
Le programme est maintenant correctement installé et executé.

### utilisation :

une fois connecté à Go-Forum vous arriverez sur la page d'acceuil :
- Elle vous permet d'accêder à un aperçu de tous les postes et de les filtrer selon plusieurs paramètres, en cliquant sur un de ces postes vous accéderez à sa page affichant le poste complet et tout ses commentaires, si c'est votre poste vous aurez la possibilité de le modifier.
- Afin de poster et commenter vous devrez être connecté sur le site.
- la Page profile est une page privé vous permettant d'accéder à vos informations personnelles et de les modifier si besoin, c'est aussi ici que vous pourrez ajouter une photo de profile.

Url d'accés au site : **http://localhost:8080**.