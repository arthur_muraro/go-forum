package forum

import (
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"text/template"
	"time"
)

func Profile(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		files := []string{
			"./views/Templates/profile.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		NewUserInfos := User{}
		if r.Method == "POST" {
			NewUserInfos = User{
				FirstName:      r.FormValue("FirstName"),
				LastName:       r.FormValue("LastName"),
				Pseudo:         r.FormValue("Pseudo"),
				Password:       "",
				Birthday:       r.FormValue("Birthday"),
				EmailAddress:   r.FormValue("EmailAddress"),
				ProfilePicture: "/userpictures/" + GetCookies(r) + ".png",
			}
		}

		if NewUserInfos.Birthday == "" {
			NewUserInfos.Birthday = GetUserInfosFromDB(database, GetCookies(r)).Birthday
		}

		fmt.Println(NewUserInfos)
		fmt.Println(GetUserInfosFromDB(database, GetCookies(r)), "test")

		if r.Method == "POST" && GetUserInfosFromDB(database, GetCookies(r)) != NewUserInfos {

			uploadFile(w, r)
			if CheckPseudo(database, NewUserInfos.Pseudo, GetUserInfosFromDB(database, GetCookies(r)).Pseudo) {
				Content := ProfileInfo{
					UserFromDB: GetUserInfosFromDB(database, GetCookies(r)),
					Message:    "Ce pseudo est déjà prit.",
				}
				view.Execute(w, ContentMerger(CheckCookies(database, r), Content))

			} else if ChangeInformations(database, NewUserInfos, GetUseridFromCookies(database, r)) {

				expiration := time.Now().Add(365 * 24 * time.Hour)
				cookie := http.Cookie{Name: "username", Value: NewUserInfos.Pseudo, Expires: expiration}
				http.SetCookie(w, &cookie)

				if CheckCookiesInDB(database, GetCookies(r)) == false {
					NewCookieTODB(database, NewUserInfos.Pseudo)
				}

				Content := ProfileInfo{
					UserFromDB: GetUserInfosFromDB(database, GetCookies(r)),
					Message:    "Les modifications ce sont correctement appliquées !",
				}
				view.Execute(w, ContentMerger(CheckCookies(database, r), Content))

			}

		} else {
			uploadFile(w, r)
			Content := ProfileInfo{
				UserFromDB: GetUserInfosFromDB(database, GetCookies(r)),
				Message:    "",
			}
			view.Execute(w, ContentMerger(CheckCookies(database, r), Content))
		}
	}
}

func GetUserInfosFromDB(db *Sqlite, UserPseudo string) User {
	rows, err := db.Db.Query("SELECT Firstname, Lastname, strftime('%Y-%m-%d', Birthday), EmailAddress FROM users WHERE Pseudo = ?", UserPseudo)
	defer rows.Close()

	if err != nil {
		print(err)
		log.Fatal(err)
	}

	var FirstName string
	var LastName string
	var Birthday string
	var EmailAddress string

	for rows.Next() {
		rows.Scan(&FirstName, &LastName, &Birthday, &EmailAddress)
	}

	UserInfos := User{
		FirstName:      html.EscapeString(FirstName),
		LastName:       html.EscapeString(LastName),
		Pseudo:         html.EscapeString(UserPseudo),
		Password:       "",
		Birthday:       html.EscapeString(Birthday),
		EmailAddress:   html.EscapeString(EmailAddress),
		ProfilePicture: "/userpictures/" + UserPseudo + ".jpeg",
	}

	return UserInfos
}

func ChangeInformations(db *Sqlite, UserInfos User, UserId string) bool {

	statement, err := db.Db.Prepare(`UPDATE users SET Firstname = ?, Lastname = ?, Pseudo = ?, Birthday = ?, EmailAddress = ? WHERE id = ?`)
	if err != nil {
		log.Fatalln(err.Error())
		return false
	}

	statement.Exec(UserInfos.FirstName, UserInfos.LastName, UserInfos.Pseudo, UserInfos.Birthday, UserInfos.EmailAddress, UserId)

	return true
}

func CheckPseudo(db *Sqlite, NewPseudo, PseudoFromDB string) bool {
	rows, err := db.Db.Query("SELECT Pseudo FROM users")
	defer rows.Close()

	if err != nil {
		print(err)
		log.Fatal(err)
	}

	var PseudoBDD string
	for rows.Next() {
		rows.Scan(&PseudoBDD)

		if PseudoBDD == NewPseudo && PseudoBDD != PseudoFromDB {
			return true
		}
	}

	return false
}

func uploadFile(w http.ResponseWriter, r *http.Request) bool {
	r.ParseMultipartForm(10 << 20)
	file, _, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return false
	}
	defer file.Close()
	tempFile, err := ioutil.TempFile("./userpictures", GetCookies(r)+".jpeg")
	if err != nil {
		fmt.Println(err)
		return false
	}
	defer os.Rename(tempFile.Name(), "./userpictures/"+GetCookies(r)+".jpeg")
	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return false
	}

	tempFile.Write(fileBytes)

	return true
}
