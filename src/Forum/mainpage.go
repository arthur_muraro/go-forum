package forum

import (
	"database/sql"
	"fmt"
	"html"
	"log"
	"net/http"
	"strings"
	"text/template"

	_ "github.com/mattn/go-sqlite3"
)

func MainPage(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		r.FormValue("Disconnect")
		if r.FormValue("Disconnect") == "yes" {
			c := http.Cookie{
				Name:   "username",
				MaxAge: -1,
			}
			http.SetCookie(w, &c)
		}

		files := []string{
			"./views/Templates/index.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		AllPosts := GetPostsFromDB(database, "*")
		if r.Method == "POST" {
			var FrontEndValues [10]string
			FrontEndValues[0] = r.FormValue("Informatique") + " "
			FrontEndValues[1] = r.FormValue("Sport") + " "
			FrontEndValues[2] = r.FormValue("Jeux_video") + " "
			FrontEndValues[3] = r.FormValue("Gastronomie") + " "
			FrontEndValues[4] = r.FormValue("Animaux") + " "
			FrontEndValues[5] = r.FormValue("Ecologie") + " "
			FrontEndValues[6] = r.FormValue("Beaute") + " "
			FrontEndValues[7] = r.FormValue("Divers") + " "
			FrontEndValues[8] = r.FormValue("Series") + " "
			FrontEndValues[9] = r.FormValue("Vetements") + " "

			if r.FormValue("Newest") != "" || r.FormValue("Oldest") != "" {
				fmt.Println(r.FormValue("Newest") + "|" + r.FormValue("Oldest"))
				AllPosts = GetPostsFromDB(database, r.FormValue("Oldest"))
			}

			if len(FrontEndValues[0]) > 1 || len(FrontEndValues[1]) > 1 || len(FrontEndValues[2]) > 1 || len(FrontEndValues[3]) > 1 || len(FrontEndValues[4]) > 1 || len(FrontEndValues[5]) > 1 || len(FrontEndValues[6]) > 1 || len(FrontEndValues[7]) > 1 || len(FrontEndValues[8]) > 1 || len(FrontEndValues[9]) > 1 {
				fmt.Println(FrontEndValues)
				AllPosts = GetPostsByCategories(AllPosts, FrontEndValues)
			}

			if r.FormValue("WorstLikesPosts") != "" || r.FormValue("MostLikedPosts") != "" {
				fmt.Println(r.FormValue("WorstLikesPosts") + "|" + r.FormValue("MostLikedPosts"))
				AllPosts = GetPostByLikes(AllPosts, r.FormValue("WorstLikesPosts"))
			}

		}
		view.Execute(w, ContentMerger(CheckCookies(database, r), AllPosts))
	}

}

func GetPostsFromDB(database *Sqlite, Index string) []Post {
	// mettre l'ID du poste voulu, ou si vous les voulez tous, mettez une string astérix en paramètres
	titrePost := []Post{}
	var Title string
	categories := GetCategoriesFromDB(database)
	var Body string
	var id int
	var date string
	var user_id string

	rows := ParamsChecker(database, Index)
	defer rows.Close()

	i := 0
	for rows.Next() {
		rows.Scan(&Title, &Body, &id, &date, &user_id)

		if Index != "*" && Index != "" && Index != "Oldest" {
			titrePost = append(titrePost, Post{
				Title:          html.EscapeString(Title),
				Body:           html.EscapeString(Body),
				Categories:     strings.Split(categories[i], ","),
				Id:             id,
				Date:           date,
				User:           GetPseudoFromId(database, user_id),
				Like:           GetLikeFromDB(database, id),
				ProfilePicture: "/userpictures/" + GetPseudoFromId(database, user_id) + ".jpeg",
			})
		} else {
			titrePost = append(titrePost, Post{
				Title:          html.EscapeString(Title),
				Body:           html.EscapeString(Body),
				Categories:     strings.Split(categories[i], ","),
				Id:             id,
				Date:           date,
				User:           GetPseudoFromId(database, user_id),
				Like:           GetLikeFromDB(database, i+1),
				ProfilePicture: "/userpictures/" + GetPseudoFromId(database, user_id) + ".jpeg",
			})
		}

		i++
	}
	return titrePost
}

func ParamsChecker(db *Sqlite, Index string) *sql.Rows {
	if Index == "*" {
		rows, err := db.Db.Query("SELECT title, body, id, strftime('%Y-%m-%d', Date), user_id FROM posts")

		if err != nil {
			log.Fatal(err)
		}

		return rows

	}

	if Index == "Oldest" {
		rows, err := db.Db.Query("SELECT title, body, id, strftime('%Y-%m-%d', Date), user_id FROM posts ORDER BY Date")

		if err != nil {
			log.Fatal(err)
		}

		return rows

	}

	if Index == "" {
		rows, err := db.Db.Query("SELECT title, body, id, strftime('%Y-%m-%d', Date), user_id FROM posts ORDER BY Date DESC")

		if err != nil {
			log.Fatal(err)
		}

		return rows

	}

	rows, err := db.Db.Query("SELECT title, body, id, strftime('%Y-%m-%d', Date), user_id FROM posts WHERE id = ?", Index)

	if err != nil {
		log.Fatal(err)
	}

	return rows
}

func GetCategoriesFromDB(db *Sqlite) []string {
	rows, err := db.Db.Query("SELECT category_name FROM category")
	defer rows.Close()

	if err != nil {
		log.Fatal(err)
	}

	var categories string
	var returnvar []string

	for rows.Next() {
		rows.Scan(&categories)
		returnvar = append(returnvar, categories)

	}
	return returnvar
}

func GetLikeFromDB(db *Sqlite, post_id int) int {
	rows, err := db.Db.Query("SELECT user_id FROM likes WHERE post_id = ?", post_id)
	defer rows.Close()
	if err != nil {
		log.Fatal(err)
	}

	NUmberOfLikes := 0
	for rows.Next() {
		rows.Scan()
		NUmberOfLikes++
	}

	return NUmberOfLikes
}
