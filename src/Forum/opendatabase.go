package forum

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

func OpenDatabase() *Sqlite {
	database, _ := sql.Open("sqlite3", "./Database/database.db")
	return &Sqlite{Db: database}
}
