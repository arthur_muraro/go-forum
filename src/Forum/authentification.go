package forum

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
	"time"

	bcrypt "golang.org/x/crypto/bcrypt"
)

func Authentification(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		AuthentificationChecker := AuthentificationInfos{
			Pseudo:   r.FormValue("Pseudo"),
			Password: r.FormValue("Password"),
		}
		fmt.Println("Ok (200)")
		fmt.Println(AuthentificationChecker)

		files := []string{
			"./views/Templates/authentification.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if AuthentificationChecker.Pseudo != "" && AuthentificationChecker.Password != "" {

			if ExistingUserChecker(database, AuthentificationChecker) {
				UserConnectedMessage := ResponseMessage{
					Message: "vous êtes connecté !",
				}

				expiration := time.Now().Add(365 * 24 * time.Hour)
				cookie := http.Cookie{Name: "username", Value: AuthentificationChecker.Pseudo, Expires: expiration}
				http.SetCookie(w, &cookie)

				if CheckCookiesInDB(database, GetCookies(r)) == false {
					NewCookieTODB(database, AuthentificationChecker.Pseudo)
				}

				view.Execute(w, ContentMerger(CheckCookies(database, r), UserConnectedMessage))

			} else {

				UserNotExist := ResponseMessage{
					Message: "Erreur d'authentification, vérifiez votre Pseudo ou Votre Mot de passe...",
				}

				view.Execute(w, ContentMerger(CheckCookies(database, r), UserNotExist))

			}

		} else {

			EmptyFieldErrorMessage := ResponseMessage{
				Message: "certains champs sont vides",
			}

			view.Execute(w, ContentMerger(CheckCookies(database, r), EmptyFieldErrorMessage))

		}
	}
}

func ExistingUserChecker(db *Sqlite, UserInfos AuthentificationInfos) bool {

	UserList, err := db.Db.Query("SELECT Pseudo, Password FROM users WHERE Pseudo = ?", UserInfos.Pseudo)
	if err != nil {
		log.Fatalln(err.Error())
	}

	var Pseudo string
	var Password string

	for UserList.Next() {
		err = UserList.Scan(&Pseudo, &Password)
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	if Pseudo == UserInfos.Pseudo && CheckPasswordHash(Password, UserInfos.Password) {
		return true
	}

	return false

}

func CheckPasswordHash(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func NewCookieTODB(db *Sqlite, Pseudo string) {
	InserCookie := `INSERT INTO cookies (
        Pseudo)
        VALUES (?)`

	statement, err := db.Db.Prepare(InserCookie)

	if err != nil {
		log.Fatalln(err.Error())
	}

	test, err := statement.Exec(Pseudo)
	fmt.Println(test)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
