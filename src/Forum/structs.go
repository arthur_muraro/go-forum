package forum

import "database/sql"

type User struct {
	FirstName      string
	LastName       string
	Pseudo         string
	Password       string
	Birthday       string
	EmailAddress   string
	ProfilePicture string
}

type AuthentificationInfos struct {
	Pseudo   string
	Password string
}

type ResponseMessage struct {
	Message string
}

type Post struct {
	Title          string
	Categories     []string
	Body           string
	Id             int
	User           string
	Date           string
	Like           int
	ProfilePicture string
}

type Comment struct {
	Title string
	Body  string
	User  string
	Date  string
}

type PostandComment struct {
	Post               []Post
	Comments           []Comment
	ModificationRights bool
}

type Sqlite struct {
	Db *sql.DB
}

type Session struct {
	Session bool
	Content interface{}
}

type NewComment struct {
	ResponseMessage string
	Id              string
}

type ProfileInfo struct {
	UserFromDB User
	Message    string
}
