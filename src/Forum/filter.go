package forum

func GetPostsByCategories(AllPosts []Post, FrontEndValues [10]string) []Post {
	var FilteredPosts []Post
	for i := 0; i <= len(AllPosts)-1; i++ {

		for j := 0; j <= len(AllPosts[i].Categories)-1; j++ {

			for k := 0; k <= len(FrontEndValues)-1; k++ {

				if AllPosts[i].Categories[j] == FrontEndValues[k] && FrontEndValues[k] != " " && FrontEndValues[k] != "" {
					print(AllPosts[i].Categories[j])

					if FilteredPosts == nil {
						FilteredPosts = append(FilteredPosts, AllPosts[i])

					} else {
						Value := true
						for l := 0; l <= len(FilteredPosts)-1; l++ {
							if AllPosts[i].Id == FilteredPosts[l].Id {
								Value = false
							}
						}
						if Value {
							FilteredPosts = append(FilteredPosts, AllPosts[i])
						}
					}
				}

			}
		}
	}

	return FilteredPosts
}

func GetPostByLikes(AllPosts []Post, SortType string) []Post {
	for Gindex := len(AllPosts) - 1; Gindex >= 1; Gindex-- {
		for Pindex := Gindex - 1; Pindex >= 0; Pindex-- {
			if SortType == "WorstLikesPosts" {
				if AllPosts[Gindex].Like < AllPosts[Pindex].Like {
					AllPosts[Gindex], AllPosts[Pindex] = AllPosts[Pindex], AllPosts[Gindex]
				}
			} else {
				if AllPosts[Gindex].Like > AllPosts[Pindex].Like {
					AllPosts[Gindex], AllPosts[Pindex] = AllPosts[Pindex], AllPosts[Gindex]
				}
			}

		}
	}
	return AllPosts
}
