package forum

import (
	"fmt"
	"log"
	"net/http"
	"text/template"

	_ "github.com/mattn/go-sqlite3"
)

func CreateComment(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		files := []string{
			"./views/Templates/comment.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if r.FormValue("Title") != "" && r.FormValue("Body") != "" {

			NewCommentIntoDB(database, r.FormValue("Id"), GetUseridFromCookies(database, r), r.FormValue("Title"), r.FormValue("Body"))

			ParentStruct := NewComment{
				ResponseMessage: "Le commentaire a bien été posté !",
				Id:              r.FormValue("Id"),
			}

			view.Execute(w, ContentMerger(CheckCookies(database, r), ParentStruct))

		} else {

			ParentStruct := NewComment{
				ResponseMessage: "Certains champs sont vides",
				Id:              r.FormValue("Id"),
			}

			view.Execute(w, ContentMerger(CheckCookies(database, r), ParentStruct))
		}

	}
}

func NewCommentIntoDB(db *Sqlite, post_id, user_id, Title, Body string) {

	print(post_id, user_id, Title, Body)

	statement, err := db.Db.Prepare("INSERT INTO comments (post_id, user_id, title, body, Date) VALUES(?, ?, ?, ?, date('now'))")
	if err != nil {
		log.Fatalln(err.Error())
	}

	test, err := statement.Exec(post_id, user_id, Title, Body)
	fmt.Println(test)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
