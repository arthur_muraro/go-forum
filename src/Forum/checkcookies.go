package forum

import (
	"log"
	"net/http"
)

func CheckCookies(database *Sqlite, r *http.Request) bool {

	return CheckCookiesInDB(database, GetCookies(r))

}

func CheckCookiesInDB(db *Sqlite, PseudoCookie string) bool {

	UserList, err := db.Db.Query("SELECT Pseudo FROM cookies")
	defer UserList.Close()

	if err != nil {
		log.Fatalln(err.Error())
	}

	var Pseudo string

	for UserList.Next() {
		err = UserList.Scan(&Pseudo)
		if err != nil {
			log.Fatalln(err.Error())
		}

		if Pseudo == PseudoCookie {
			return true
		}

	}

	return false
}

func GetCookies(r *http.Request) string {

	cookie, err := r.Cookie("username")
	if err != nil {
		return ""
	}
	return cookie.Value
}

func ContentMerger(session bool, content interface{}) Session {
	Content := Session{
		Session: session,
		Content: content,
	}

	return Content
}

func GetUseridFromCookies(db *Sqlite, r *http.Request) string {
	rows, err := db.Db.Query("SELECT Id FROM users WHERE Pseudo = ?", GetCookies(r))
	defer rows.Close()

	if err != nil {
		print(err)
		log.Fatal(err)
	}

	var user_id string
	for rows.Next() {
		rows.Scan(&user_id)
	}

	return user_id

}
