package forum

import (
	"html"
	"log"
	"net/http"
	"text/template"
)

func DisplayComment(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		files := []string{
			"./views/Templates/post.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if err != nil {
			log.Fatal(err)
		}

		if r.Method == "POST" {
			AddLike(database, r.FormValue("Id"), GetUseridFromCookies(database, r), CheckLike(database, r.FormValue("Id"), GetUseridFromCookies(database, r)))
		}

		Rights := false
		if GetCookies(r) == GetPostsFromDB(database, r.FormValue("Id"))[0].User {
			Rights = true
		}

		ContenuPostandComment := PostandComment{
			Post:               GetPostsFromDB(database, r.FormValue("Id")),
			Comments:           GetCommentFromDB(database, r.FormValue("Id")),
			ModificationRights: Rights,
		}

		view.Execute(w, ContentMerger(CheckCookies(database, r), ContenuPostandComment))

	}
}

func GetIDFromTitleFromDB(db *Sqlite, Title string) string {
	rows, err := db.Db.Query("SELECT id FROM posts WHERE title = ?", Title)
	defer rows.Close()

	if err != nil {
		print(err)
		log.Fatal(err)
	}

	var posts_id string
	for rows.Next() {
		rows.Scan(&posts_id)
	}

	return posts_id
}

func GetCommentFromDB(db *Sqlite, post_id string) []Comment {
	rows, err := db.Db.Query("SELECT title, body, user_id, strftime('%d-%m-%Y', Date) FROM comments WHERE post_id = ?", post_id)
	defer rows.Close()

	if err != nil {
		log.Fatal(err)
	}

	ContenuComment := []Comment{}
	var Title string
	var Body string
	var user_id string
	var Date string

	for rows.Next() {
		// rows.Scan(&TitlePost, &BodyPost, &Informatique, &Sport, &Jeux_video, &Gastronomie, &Animaux, &TitleComment, &BodyComment)
		rows.Scan(&Title, &Body, &user_id, &Date)
		ContenuComment = append(ContenuComment, Comment{
			Title: html.EscapeString(Title),
			Body:  html.EscapeString(Body),
			User:  GetPseudoFromId(db, user_id),
			Date:  html.EscapeString(Date),
		})
	}

	return ContenuComment
}

func GetPseudoFromId(db *Sqlite, user_id string) string {
	rows, err := db.Db.Query("SELECT Pseudo FROM users WHERE Id = ?", user_id)
	defer rows.Close()

	if err != nil {
		log.Fatal(err)
	}

	var Pseudo string

	for rows.Next() {
		rows.Scan(&Pseudo)
	}

	return Pseudo
}

func AddLike(db *Sqlite, post_id, user_id string, Param bool) {
	var insert string
	if Param {
		insert = (`INSERT INTO likes (
			post_id, user_id)
			VALUES (?, ?)`)
	} else {
		insert = (`DELETE FROM likes WHERE post_id = ? AND user_id = ?`)
	}

	statement, err := db.Db.Prepare(insert)
	if err != nil {
		log.Fatalln(err.Error())
	}

	_, err = statement.Exec(post_id, user_id)
	if err != nil {
		log.Fatalln(err.Error())

	}

}

func CheckLike(db *Sqlite, post_id, user_id string) bool {
	rows, err := db.Db.Query("SELECT user_id FROM likes WHERE post_id = ?", post_id)
	defer rows.Close()
	if err != nil {
		log.Fatal(err)
	}

	// check like in DB

	if err != nil {
		log.Fatal(err)
	}

	var Db_user_id string

	for rows.Next() {
		rows.Scan(&Db_user_id)
		if Db_user_id == user_id {
			return false
		}
	}

	return true
}
