package forum

import (
	"fmt"
	"log"
	"net/http"
	"text/template"

	_ "github.com/mattn/go-sqlite3"
	bcrypt "golang.org/x/crypto/bcrypt"
)

func CreateUser(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		NewUserToCreate := User{
			FirstName:    r.FormValue("FirstName"),
			LastName:     r.FormValue("LastName"),
			Pseudo:       r.FormValue("Pseudo"),
			Password:     HashPassword(r.FormValue("Password")),
			Birthday:     r.FormValue("Birthday"),
			EmailAddress: r.FormValue("EmailAddress"),
		}
		fmt.Println("Ok (200)")
		fmt.Println(NewUserToCreate)

		files := []string{
			"./views/Templates/create_user.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if NewUserToCreate.FirstName != "" && NewUserToCreate.LastName != "" && NewUserToCreate.Pseudo != "" && NewUserToCreate.Password != "" && NewUserToCreate.Birthday != "" && NewUserToCreate.EmailAddress != "" {
			NewUserIntoDB(database, NewUserToCreate)
			CompteCréé := ResponseMessage{
				Message: "le Compte a été correctemment créé !",
			}
			view.Execute(w, ContentMerger(CheckCookies(database, r), CompteCréé))

		} else {
			ErrorMessage := ResponseMessage{
				Message: "certains champs sont vides",
			}

			view.Execute(w, ContentMerger(CheckCookies(database, r), ErrorMessage))
		}

	}
}

func NewUserIntoDB(db *Sqlite, UserInfos User) {
	InsertUser := `INSERT INTO users (
        FirstName, LastName, Pseudo, Password, Birthday, EmailAddress)
        VALUES (?, ?, ?, ?, ?, ?)`

	statement, err := db.Db.Prepare(InsertUser)

	if err != nil {
		log.Fatalln(err.Error())
	}

	test, err := statement.Exec(UserInfos.FirstName, UserInfos.LastName, UserInfos.Pseudo, UserInfos.Password, UserInfos.Birthday, UserInfos.EmailAddress)
	fmt.Println(test)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func HashPassword(password string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes)
}
