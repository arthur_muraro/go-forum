package forum

import (
	"fmt"
	"log"
	"net/http"
	"text/template"

	_ "github.com/mattn/go-sqlite3"
)

func CreatePost(database *Sqlite) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		NewPostToCreate := Post{
			Title: r.FormValue("title"),
			Body:  r.FormValue("body"),
		}

		files := []string{
			"./views/Templates/newpost.html",
			"./views/Templates/header.html",
			"./views/Templates/base_tmpl.html",
		}

		view, err := template.ParseFiles(files...)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if r.Method == "POST" {
			if r.FormValue("title") != "" && r.FormValue("Informatique") != "" || r.FormValue("Sport") != "" || r.FormValue("Jeux_video") != "" || r.FormValue("Gastronomie") != "" || r.FormValue("Animaux") != "" || r.FormValue("Ecologie") != "" || r.FormValue("Beaute") != "" || r.FormValue("Divers") != "" || r.FormValue("Series") != "" || r.FormValue("Vetements") != "" && r.FormValue("body") != "" {
				NewPostIntoDB(database, GetUseridFromCookies(database, r), r.FormValue("Id"), NewPostToCreate)
				JV := r.FormValue("Jeux_video")
				if JV != "" {
					JV = "Jeux Vidéo"
				}
				NewCategoriesIntoDB(database, r.FormValue("Informatique")+" ,"+r.FormValue("Sport")+" ,"+JV+" ,"+r.FormValue("Gastronomie")+" ,"+r.FormValue("Animaux")+" ,"+r.FormValue("Ecologie")+" ,"+r.FormValue("Beaute")+" ,"+r.FormValue("Divers")+" ,"+r.FormValue("Series")+" ,"+r.FormValue("Vetements")+" ", r.FormValue("Id"))
				postcreated := NewComment{
					ResponseMessage: "le poste a bien été créé.",
					Id:              r.FormValue("Id"),
				}

				view.Execute(w, ContentMerger(CheckCookies(database, r), postcreated))
			} else {
				ErrorMessage := NewComment{
					ResponseMessage: "certains champs sont vides, vous devez mettre un titre et un contenu. Au moins une catégorie doit être choisie.",
					Id:              r.FormValue("Id"),
				}

				view.Execute(w, ContentMerger(CheckCookies(database, r), ErrorMessage))
			}

		} else {
			InitMessage := NewComment{
				ResponseMessage: "",
				Id:              r.FormValue("Id"),
			}

			view.Execute(w, ContentMerger(CheckCookies(database, r), InitMessage))
		}

	}

}

func NewPostIntoDB(db *Sqlite, user_id, Param string, PostInfos Post) {
	if Param == "0" {
		fmt.Println("mauvais statement")

		InsertPost := `INSERT INTO posts (
			user_id, title, body, Date)
			VALUES (?, ?, ?, date('now'))`

		statement, err := db.Db.Prepare(InsertPost)
		if err != nil {
			log.Fatalln(err.Error())
		}

		statement.Exec(user_id, PostInfos.Title, PostInfos.Body)

	} else {
		fmt.Println("bon statement")

		statement, err := db.Db.Prepare(`UPDATE posts SET title = ?, body = ?, Date = date('now') WHERE id = ?`)
		if err != nil {
			log.Fatalln(err.Error())
		}

		statement.Exec(PostInfos.Title, PostInfos.Body, Param)

	}
}

func NewCategoriesIntoDB(db *Sqlite, Categories, Param string) {
	if Param == "0" {
		Insertcategory := `INSERT INTO category (
			category_name)
			VALUES (?)`

		statement, err := db.Db.Prepare(Insertcategory)
		if err != nil {
			log.Fatalln(err.Error())
		}

		statement.Exec(Categories)
	} else {
		statement, err := db.Db.Prepare(`UPDATE category SET category_name = ? WHERE id = ?`)
		if err != nil {
			log.Fatalln(err.Error())
		}

		statement.Exec(Categories, Param)
	}
}
