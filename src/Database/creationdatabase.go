package main

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func CreateUserInDB(db *sql.DB) {
	UserTable := `CREATE TABLE IF NOT EXISTS users (
        "Id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "Firstname" VARCHAR(100) NOT NULL,
        "Lastname" VARCHAR(100) NOT NULL,
        "Pseudo" VARCHAR(100) NOT NULL UNIQUE,
        "Birthday" DATE,
        "EmailAddress" VARCHAR(100) NOT NULL,
        "Password" VARCHAR(100) NOT NULL
    );`

	statement, err := db.Prepare(UserTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("User table created")

}

func CreateCommentInDB(db *sql.DB) {
	CommentTable := `CREATE TABLE IF NOT EXISTS comments (
        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "post_id" INTEGER NOT NULL,
        "user_id" INTEGER NOT NULL,
		"title" VARCHAR(100),
        "body" TEXT NOT NULL,      
		"Date" DATETIME,  
        FOREIGN KEY    (user_id) REFERENCES users(id)
        FOREIGN KEY    (post_id) REFERENCES posts(id)        
    );`

	statement, err := db.Prepare(CommentTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("Comments table created")

}

func CreatePostInDB(db *sql.DB) {
	PostTable := `CREATE TABLE IF NOT EXISTS posts (
        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "user_id" INTEGER NOT NULL,
        "title" VARCHAR(100) NOT NULL,
        "body" TEXT NOT NULL,
		"Date" DATETIME,
        FOREIGN KEY (user_id) REFERENCES users(id)
    );`

	statement, err := db.Prepare(PostTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("post table created")

}

func CreateCategoryInDB(db *sql.DB) {
	CategoryTable := `CREATE TABLE IF NOT EXISTS category (
        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"category_name" VARCHAR(100)
    );`

	statement, err := db.Prepare(CategoryTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("Category table created")

}

func CreateCategoryPostInDB(db *sql.DB) {
	PostCategoryTable := `CREATE TABLE IF NOT EXISTS categorypost (
		"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"post_id" INTEGER NOT NULL,
		"category_id" INTEGER NOT NULL,
		FOREIGN KEY (post_id) REFERENCES posts(id)
		FOREIGN KEY (category_id) REFERENCES category(id)
    );`

	statement, err := db.Prepare(PostCategoryTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("PostCategory table created")
}

func CreateLikeInDB(db *sql.DB) {
	LikeTable := `CREATE TABLE IF NOT EXISTS likes (
        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "post_id" INTEGER NOT NULL,
        "user_id" INTEGER NOT NULL
    );`

	statement, err := db.Prepare(LikeTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("Likes table created")

}

func CreateDisLikeInDB(db *sql.DB) {
	LikeTable := `CREATE TABLE IF NOT EXISTS Dislikes (
        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "post_id" INTEGER NOT NULL,
        "user_id" INTEGER NOT NULL
    );`

	statement, err := db.Prepare(LikeTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("Likes table created")

}

func CreateCookieinDB(db *sql.DB) {

	cookieTable := `CREATE TABLE IF NOT EXISTS cookies (
        "Pseudo" STRING NOT NULL
    );`

	statement, err := db.Prepare(cookieTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("cks table created")
}

func main() {
	database, _ := sql.Open("sqlite3", "./database.db")

	// Create SQLite file
	log.Println("On creer le fichier database.db")
	file, err := os.Create("database.db")
	if err != nil {
		log.Fatal(err.Error())
	}
	file.Close()
	log.Println("database.db created")

	CreateUserInDB(database)
	CreatePostInDB(database)
	CreateCommentInDB(database)
	CreateLikeInDB(database)
	CreateCookieinDB(database)
	CreateCategoryInDB(database)
	CreateCategoryPostInDB(database)
	CreateDisLikeInDB(database)
}
