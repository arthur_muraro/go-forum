package main

import (
	"log"
	"net/http"

	Forum "Forum/Forum"
)

//Création de route pour chaque page web afin d'effectuer chaqu'une fonctions
func main() {
	database := Forum.OpenDatabase()

	css := http.FileServer(http.Dir("./views/css"))
	http.Handle("/css/", http.StripPrefix("/css/", css))

	images := http.FileServer(http.Dir("./views/images"))
	http.Handle("/images/", http.StripPrefix("/images/", images))

	PP := http.FileServer(http.Dir("./userpictures"))
	http.Handle("/userpictures/", http.StripPrefix("/userpictures/", PP))

	http.HandleFunc("/", Forum.MainPage(database))
	// c'est la main page, elle affiche les postes.

	http.HandleFunc("/profile", Forum.Profile(database))
	// affiche les information de l'uilisateur.

	http.HandleFunc("/createuser", Forum.CreateUser(database))
	// permet de creer un utilisateur

	http.HandleFunc("/authentification", Forum.Authentification(database))
	// permet de se connecter

	http.HandleFunc("/newpost", Forum.CreatePost(database))
	// permet de creer un nouveau poste.

	http.HandleFunc("/newcomment", Forum.CreateComment(database))
	// permet de creer un nouveau commentaire.

	http.HandleFunc("/post", Forum.DisplayComment(database))
	// elle affiche le post spécifié, et ses commentaires.

	// http.HandleFunc("/like", Forum.CreateLike)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
